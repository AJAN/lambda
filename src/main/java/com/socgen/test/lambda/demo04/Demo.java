package com.socgen.test.lambda.demo04;

import java.math.BigDecimal;
import java.util.List;
import java.util.function.BinaryOperator;

public class Demo {

//    public BigDecimal sum(final List<BigDecimal> values) {
//        return this.operate(values, (first, second) -> first.add(second));
//    }

//    public BigDecimal sum(final List<BigDecimal> values) {
//        return this.operate(values, this::sum);
//    }
//
//    BigDecimal sum(final BigDecimal first, final BigDecimal second) {
//        return first.add(second);
//    }

    public BigDecimal sum(final List<BigDecimal> values) {
        return this.operate(values, BigDecimal::add);
    }

    public BigDecimal subtract(final List<BigDecimal> values) {
        return this.operate(values, BigDecimal::subtract);
    }

    public BigDecimal multiply(final List<BigDecimal> values) {
        return this.operate(values, BigDecimal::multiply);
    }

    BigDecimal operate(final List<BigDecimal> values, final BinaryOperator<BigDecimal> operation) {
        BigDecimal result = null;
        if (values != null) {
            for (final BigDecimal value : values) {
                if (result == null) {
                    result = value;
                } else if (value != null) {
                    result = operation.apply(result, value);
                }
            }
        }
        return result;
    }

}
