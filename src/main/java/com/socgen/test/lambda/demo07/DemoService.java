package com.socgen.test.lambda.demo07;

import java.util.List;
import java.util.Objects;
import java.util.function.BinaryOperator;

public class DemoService {

    public Integer sum(final List<Integer> values) {
        return this.operate(values, this::add);
    }

    Integer operate(final List<Integer> values, final BinaryOperator<Integer> operation) {
        if (values == null) {
            return null;
        }
        return values.stream().filter(Objects::nonNull).reduce(operation).orElse(null);
    }

    Integer add(final Integer first, final Integer second) {
        throw new RuntimeException("not yet implemented");
    }

}
