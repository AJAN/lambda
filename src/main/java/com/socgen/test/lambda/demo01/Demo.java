package com.socgen.test.lambda.demo01;

import java.math.BigDecimal;
import java.util.List;

public class Demo {

    public BigDecimal sum(final List<BigDecimal> values) {
        BigDecimal result = null;
        if (values != null) {
            for (final BigDecimal value : values) {
                if (result == null) {
                    result = value;
                } else if (value != null) {
                    result = result.add(value);
                }
            }
        }
        return result;
    }

    public BigDecimal subtract(final List<BigDecimal> values) {
        BigDecimal result = null;
        if (values != null) {
            for (final BigDecimal value : values) {
                if (result == null) {
                    result = value;
                } else if (value != null) {
                    result = result.subtract(value);
                }
            }
        }
        return result;
    }

    public BigDecimal multiply(final List<BigDecimal> values) {
        BigDecimal result = null;
        if (values != null) {
            for (final BigDecimal value : values) {
                if (result == null) {
                    result = value;
                } else if (value != null) {
                    result = result.multiply(value);
                }
            }
        }
        return result;
    }

}
