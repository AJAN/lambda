package com.socgen.test.lambda.demo05;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.function.BinaryOperator;

public class Demo {

    public BigDecimal sum(final List<BigDecimal> values) {
        return this.operate(values, BigDecimal::add);
    }

    public BigDecimal subtract(final List<BigDecimal> values) {
        return this.operate(values, BigDecimal::subtract);
    }

    public BigDecimal multiply(final List<BigDecimal> values) {
        return this.operate(values, BigDecimal::multiply);
    }

    BigDecimal operate(final List<BigDecimal> values, final BinaryOperator<BigDecimal> operation) {
        if (values == null) {
            return null;
        }
        return values.stream().filter(Objects::nonNull).reduce(operation).orElse(null);
    }

}
