package com.socgen.test.lambda.demo03;

import java.math.BigDecimal;

@FunctionalInterface
interface Operation {

    BigDecimal operate(BigDecimal first, BigDecimal second);

}