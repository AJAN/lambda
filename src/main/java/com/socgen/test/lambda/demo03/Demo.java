package com.socgen.test.lambda.demo03;

import java.math.BigDecimal;
import java.util.List;

public class Demo {

    public BigDecimal sum(final List<BigDecimal> values) {
        return this.operate(values, new Operation() {
            @Override
            public BigDecimal operate(BigDecimal first, BigDecimal second) {
                return first.add(second);
            }
        });
    }

    public BigDecimal subtract(final List<BigDecimal> values) {
        return this.operate(values, (first, second) -> first.subtract(second));
    }

    public BigDecimal multiply(final List<BigDecimal> values) {
        return this.operate(values, (first, second) -> first.multiply(second));
    }

    BigDecimal operate(final List<BigDecimal> values, final Operation operation) {
        BigDecimal result = null;
        if (values != null) {
            for (final BigDecimal value : values) {
                if (result == null) {
                    result = value;
                } else if (value != null) {
                    result = operation.operate(result, value);
                }
            }
        }
        return result;
    }

}
