package com.socgen.test.lambda.demo06;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Demo {

    class Person {
        private boolean female;
        private String name;
        private int age;

        public String getName() {
            return this.name;
        }

        public void setName(final String name) {
            this.name = name;
        }

        public int getAge() {
            return this.age;
        }

        public void setAge(final int age) {
            this.age = age;
        }

        public boolean isFemale() {
            return this.female;
        }

        public void setFemale(final boolean male) {
            this.female = male;
        }
    }

    public List<String> nameOfAdultWomenOrderedByAge(final List<Person> persons) {
        return persons.stream()
                .filter(person -> person.getAge() >= 18)
                .filter(Person::isFemale)
                .sorted(Comparator.comparing(Person::getAge))
                .map(Person::getName)
                .collect(Collectors.toList());
    }

}
