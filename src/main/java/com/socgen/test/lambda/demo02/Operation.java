package com.socgen.test.lambda.demo02;

import java.math.BigDecimal;

interface Operation {
    BigDecimal operate(BigDecimal first, BigDecimal second);
}