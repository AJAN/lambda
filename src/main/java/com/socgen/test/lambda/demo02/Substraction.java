package com.socgen.test.lambda.demo02;

import java.math.BigDecimal;

class Substraction implements Operation {
    @Override
    public BigDecimal operate(final BigDecimal first, final BigDecimal second) {
        return first.subtract(second);
    }
}