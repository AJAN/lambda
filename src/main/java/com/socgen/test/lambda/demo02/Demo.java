package com.socgen.test.lambda.demo02;

import java.math.BigDecimal;
import java.util.List;

public class Demo {

    public BigDecimal sum(final List<BigDecimal> values) {
        return this.operate(values, new Addition());
    }

    public BigDecimal subtract(final List<BigDecimal> values) {
        return this.operate(values, new Substraction());
    }

    public BigDecimal multiply(final List<BigDecimal> values) {
        return this.operate(values, new Multiplication());
    }

    BigDecimal operate(final List<BigDecimal> values, final Operation operation) {
        BigDecimal result = null;
        if (values != null) {
            for (final BigDecimal value : values) {
                if (result == null) {
                    result = value;
                } else if (value != null) {
                    result = operation.operate(result, value);
                }
            }
        }
        return result;
    }

}
