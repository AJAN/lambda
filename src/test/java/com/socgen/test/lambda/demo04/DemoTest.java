package com.socgen.test.lambda.demo04;

import static java.math.BigDecimal.valueOf;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import com.socgen.test.lambda.demo02.Demo;

public class DemoTest {

    private final Demo test = new Demo();

    @Test
    public void sum() throws Exception {
        assertNull(this.test.sum(null));
        assertNull(this.test.sum(emptyList()));
        assertEquals(valueOf(100), this.test.sum(asList(valueOf(100))));
        assertEquals(valueOf(100), this.test.sum(asList(null, valueOf(100))));
        assertEquals(valueOf(100), this.test.sum(asList(valueOf(50), valueOf(50))));
    }

    @Test
        public void subtract() throws Exception {
            assertNull(this.test.subtract(null));
            assertNull(this.test.subtract(emptyList()));
            assertEquals(valueOf(100), this.test.subtract(asList(valueOf(100))));
            assertEquals(valueOf(100), this.test.subtract(asList(null, valueOf(100))));
            assertEquals(valueOf(0), this.test.subtract(asList(valueOf(50), valueOf(50))));
        }

    @Test
    public void multiply() throws Exception {
        assertNull(this.test.multiply(null));
        assertNull(this.test.multiply(emptyList()));
        assertEquals(valueOf(100), this.test.multiply(asList(valueOf(100))));
        assertEquals(valueOf(100), this.test.multiply(asList(null, valueOf(100))));
        assertEquals(valueOf(100), this.test.multiply(asList(valueOf(10), valueOf(10))));
    }

}
