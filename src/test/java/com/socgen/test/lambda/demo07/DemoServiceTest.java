package com.socgen.test.lambda.demo07;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.List;
import java.util.function.BinaryOperator;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

public class DemoServiceTest {

    @Spy
    @InjectMocks
    private DemoService test;

    @Test
    public void sum() throws Exception {
        MockitoAnnotations.initMocks(this);

        final List<Integer> values = asList(50, 51);
        final ArgumentCaptor<BinaryOperator<Integer>> captor = captor(BinaryOperator.class);

        doReturn(13).when(this.test).operate(eq(values), captor.capture()); // Mock underlying service
        assertThat(this.test.sum(values), is(13)); // Test service
        final BinaryOperator<Integer> operator = captor.getValue(); // Retrieve implementation
        verify(this.test).operate(eq(values), eq(operator)); // Verify underlying service call
        doReturn(10).when(this.test).add(11, 12); // Mock underlying method
        assertThat(operator.apply(11, 12), is(10)); // Test implementation
        verify(this.test).add(11, 12); // Verify underlying method call
    }

    @SuppressWarnings("unchecked")
    static <T> ArgumentCaptor<T> captor(final Class<?> clazz) {
        return (ArgumentCaptor<T>) ArgumentCaptor.forClass(clazz);
    }

}
